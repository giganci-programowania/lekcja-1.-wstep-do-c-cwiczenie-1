﻿namespace lekcja_1._wstep_do_c_cwiczenie_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nUDPierwsza = new System.Windows.Forms.NumericUpDown();
            this.nUDDruga = new System.Windows.Forms.NumericUpDown();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnOdejmij = new System.Windows.Forms.Button();
            this.btnPomnoz = new System.Windows.Forms.Button();
            this.btnPodziel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtWynik = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nUDPierwsza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDDruga)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pierwsza liczba";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Druga liczba";
            // 
            // nUDPierwsza
            // 
            this.nUDPierwsza.Location = new System.Drawing.Point(117, 12);
            this.nUDPierwsza.Name = "nUDPierwsza";
            this.nUDPierwsza.Size = new System.Drawing.Size(120, 20);
            this.nUDPierwsza.TabIndex = 2;
            // 
            // nUDDruga
            // 
            this.nUDDruga.Location = new System.Drawing.Point(117, 38);
            this.nUDDruga.Name = "nUDDruga";
            this.nUDDruga.Size = new System.Drawing.Size(120, 20);
            this.nUDDruga.TabIndex = 3;
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(35, 77);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 23);
            this.btnDodaj.TabIndex = 4;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnOdejmij
            // 
            this.btnOdejmij.Location = new System.Drawing.Point(162, 77);
            this.btnOdejmij.Name = "btnOdejmij";
            this.btnOdejmij.Size = new System.Drawing.Size(75, 23);
            this.btnOdejmij.TabIndex = 5;
            this.btnOdejmij.Text = "Odejmij";
            this.btnOdejmij.UseVisualStyleBackColor = true;
            this.btnOdejmij.Click += new System.EventHandler(this.btnOdejmij_Click);
            // 
            // btnPomnoz
            // 
            this.btnPomnoz.Location = new System.Drawing.Point(35, 106);
            this.btnPomnoz.Name = "btnPomnoz";
            this.btnPomnoz.Size = new System.Drawing.Size(75, 23);
            this.btnPomnoz.TabIndex = 6;
            this.btnPomnoz.Text = "Pomnóż";
            this.btnPomnoz.UseVisualStyleBackColor = true;
            this.btnPomnoz.Click += new System.EventHandler(this.btnPomnoz_Click);
            // 
            // btnPodziel
            // 
            this.btnPodziel.Location = new System.Drawing.Point(162, 106);
            this.btnPodziel.Name = "btnPodziel";
            this.btnPodziel.Size = new System.Drawing.Size(75, 23);
            this.btnPodziel.TabIndex = 7;
            this.btnPodziel.Text = "Podziel";
            this.btnPodziel.UseVisualStyleBackColor = true;
            this.btnPodziel.Click += new System.EventHandler(this.btnPodziel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Wynik";
            // 
            // txtWynik
            // 
            this.txtWynik.Location = new System.Drawing.Point(137, 152);
            this.txtWynik.Name = "txtWynik";
            this.txtWynik.Size = new System.Drawing.Size(100, 20);
            this.txtWynik.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 191);
            this.Controls.Add(this.txtWynik);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnPodziel);
            this.Controls.Add(this.btnPomnoz);
            this.Controls.Add(this.btnOdejmij);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.nUDDruga);
            this.Controls.Add(this.nUDPierwsza);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.nUDPierwsza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDDruga)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nUDPierwsza;
        private System.Windows.Forms.NumericUpDown nUDDruga;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnOdejmij;
        private System.Windows.Forms.Button btnPomnoz;
        private System.Windows.Forms.Button btnPodziel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtWynik;
    }
}

